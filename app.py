import config
import requests
import csv
from pathlib import Path


def getIMDBid(title, year): #returns unique IMDb movie identification code, returns -1 if title not found

    url = "https://movie-database-imdb-alternative.p.rapidapi.com/"

    querystring = {"s":title,"page":"1","y":year,"r":"json"}
    
    headers = {
        'x-rapidapi-key': config.api_key,
        'x-rapidapi-host': "movie-database-imdb-alternative.p.rapidapi.com"
        }
    
    response = requests.request("GET", url, headers=headers, params=querystring)
    
    try:
        return response.json()["Search"][0]["imdbID"]
    except Exception:
        return -1

def lookup(IMDBid): #returns list of services specified film is availble on
    
    url = "https://utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com/idlookup"
    
    querystring = {"source_id": IMDBid,"source":"imdb","country":"uk"}
    
    headers = {
        'x-rapidapi-key': config.api_key,
        'x-rapidapi-host': "utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com"
        }
    
    response = requests.request("GET", url, headers=headers, params=querystring)
    
    services = []
    
    try:
        locations = response.json()["collection"]["locations"]
        
        for loaction in locations:
            services.append(loaction["display_name"])
            
    except Exception:
        pass
    return services

def getServices(title, year): 
    
    tag = getIMDBid(title, year)
    
    if tag == -1:
        return 
    else:
        return lookup(tag)

def compileWatchlist(fname):
    totalrows = sum(1 for _ in open(fname))
    
    with open(fname, mode='r') as csv_in:
        
        with open(str(Path(fname).with_suffix(''))+"_updated.csv", mode='w') as csv_out:
            
            csv_reader = csv.DictReader(csv_in)
            
            fields = csv_reader.fieldnames
            fields.append("Available On")
            
            csv_writer = csv.DictWriter(csv_out, fieldnames=fields)
            csv_writer.writeheader()
            
            i = 1
            
            for row in csv_reader:
                print("[{}/{}]".format(i, totalrows))
                i+=1
                
                services = getServices(row['Name'], row['Year'])
                
                if services:
                    row['Available On'] = ', '.join(services)
                else:
                    row['Available On'] = ''
                    
                csv_writer.writerow(row)
            
            csv_out.close()
            csv_in.close()
            print("Complete")
    
compileWatchlist("watchlist.csv") 