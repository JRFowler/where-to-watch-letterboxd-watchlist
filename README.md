# Where to Watch Letterboxd Watchlist 

This is a simple application to take a letterboxd watchlist exported as a csv file and add local streaming services available for each title in  a new column

**Required APIs**
- https://rapidapi.com/rapidapi/api/movie-database-imdb-alternative
- https://rapidapi.com/utelly/api/Utelly
